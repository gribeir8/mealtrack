### SETUP

Run following commands:

$ ionic start mealtrack blank

$ cd mealtrack

$ git init

$ git remote add origin https://bitbucket.org/gribeir8/mealtrack.git

$ git fetch --all

$ git reset --hard origin/master

ionic setup sass

bower install ngCordova 

bower install angular-moment 

bower install parse-angular-patch

bower install angular-messages

ionic plugin add org.apache.cordova.camera
