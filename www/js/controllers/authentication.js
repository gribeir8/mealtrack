var app = angular.module('mealtrack.controllers.authentication', []);

/*********************************************************************
 * LoginCtrl
 *********************************************************************/
app.controller('LoginCtrl', function ($scope, $state, AuthService) {

	$scope.formData = {
		"email": "",
		"password": ""
	};

	$scope.login = function (form) {
		if(form.$valid){
			console.log("LoginCtrl::login");
			AuthService.login($scope.formData.email, $scope.formData.password).then(function(user){

				console.log('promisse: user: ' + user)
				$state.go("tab.meals");
			});
		}
	};

});

/*********************************************************************
 * SignupCtrl
 *********************************************************************/
app.controller('SignupCtrl', function ($scope, $state, AuthService) {

	$scope.formData = {
		"name": "",
		"email": "",
		"password": ""
	};

	$scope.signup = function (form) {
		console.log("SignupCtrl::signup");
		if(form.$valid){
			AuthService.signup($scope.formData.email, $scope.formData.name, $scope.formData.password).then(function(user){

				$state.go("tab.meals");
			});

		}else{
			console.log("Invalid form");
		}
	};

});